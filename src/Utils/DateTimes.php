<?php

declare(strict_types=1);

namespace Handy\Utils;

class DateTimes
{

    /**
     * Convert a date string from one format to another.
     *
     * @param  $string
     * @param  string $fromFormat
     * @param  string $toFormat
     * @return string
     */
    public static function convertDateFormat($string, $fromFormat = 'Y-m-d', $toFormat = 'F j, Y')
    {
        $date = \DateTime::createFromFormat($fromFormat, $string);
        return ($date instanceof \DateTime) ? $date->format($toFormat) : '';
    }
}
