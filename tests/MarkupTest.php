<?php

namespace Handy\Tests;

use PHPUnit\Framework\TestCase;
use Handy\Utils\Markup;

/**
 * Class MarkupTest
 *
 * @package Handy\Tests
 */
class MarkupTest extends TestCase
{
    /**
     * @test
     */
    public function cleanTest()
    {
        $markup = '<em style="font-weight: bold;">Eligendi ultricies</em> donec <div content="something">class harum corporis</div>';
        $clean = Markup::clean($markup);
        $this->assertSame('<em>Eligendi ultricies</em> donec class harum corporis', $clean);
        $noTags = Markup::clean($markup, []);
        $this->assertSame('Eligendi ultricies donec class harum corporis', $noTags);
    }

    /**
     * @test
     */
    public function getUrlTest()
    {
        $markup = '<a href="https://gitlab.com/ericpugh/handy">Download handy utils</a>';
        $url = Markup::getUrl($markup);
        $this->assertSame('https://gitlab.com/ericpugh/handy', $url);
    }

}
