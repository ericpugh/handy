<?php

namespace Handy\Tests;

use Handy\Utils\Strings;
use Handy\Utils\Validators;
use PHPUnit\Framework\TestCase;

/**
 * Class StringsTest
 *
 * @package Handy\Tests
 */
class ValidatorsTest extends TestCase
{
    /**
     * @test
     */
    public function isNullTest()
    {
        $test = '';
        $this->assertTrue(Validators::isNull($test));
        $test = 'null';
        $this->assertTrue(Validators::isNull($test));
        $test = 'this is a string';
        $this->assertFalse(Validators::isNull($test));
        $test = 123;
        $this->assertFalse(Validators::isNull($test));
    }

}
