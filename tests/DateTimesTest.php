<?php

namespace Handy\Tests;

use Handy\Utils\DateTimes;
use PHPUnit\Framework\TestCase;

/**
 * Class DateTimesTest
 *
 * @package Handy\Tests
 */
class DateTimesTest extends TestCase
{
    /**
     * @test
     */
    public function flattenTest()
    {
        $this->assertSame('April 28, 1984', DateTimes::convertDateFormat('1984-04-28'));
        $this->assertSame('April 28, 1984', DateTimes::convertDateFormat('1984/04/28', 'Y/m/d'));
    }
}
