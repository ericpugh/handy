<?php

namespace Handy\Tests;

use PHPUnit\Framework\TestCase;
use Handy\Utils\Files;

/**
 * Class FilesTest
 *
 * @package Handy\Tests
 */
class FilesTest extends TestCase
{
    /**
     * @test
     */
    public function loadFileTest()
    {
        $filename = dirname(__DIR__, 1) . '/README.md';
        $contents = Files::loadFile($filename);
        $this->assertStringEqualsFile($filename, $contents);
    }

}
