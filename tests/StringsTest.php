<?php

namespace Handy\Tests;

use Handy\Utils\Strings;
use PHPUnit\Framework\TestCase;

/**
 * Class StringsTest
 *
 * @package Handy\Tests
 */
class StringsTest extends TestCase
{
    /**
     * @test
     */
    public function truncateTest()
    {
        $markup = 'Saepe assumenda harum. Per tempor labore, corrupti quod proident officia id morbi? Sociosqu ornare
        litora, occaecati parturient nihil maxime leo porro quos? Parturient exercitation taciti diam. Est, scelerisque,
        fermentum ex, quaerat praesent?';
        $expected = 'Saepe assumenda harum.';
        $actual = Strings::truncateText($markup, 22);
        $this->assertSame($expected, $actual);
    }

    /**
     * @test
     */
    public function caseConvertTest()
    {
        $text = 'Saepe assumenda harum. Per tempor labore?';
        $title = 'SaepeAssumendaHarumPerTemporLabore';
        $camel = 'saepeAssumendaHarumPerTemporLabore';
        $snake = 'saepe_assumenda_harum_per_tempor_labore';
        $kebab = 'saepe-assumenda-harum-per-tempor-labore';
        $this->assertSame($title, Strings::titleCase($text));
        $this->assertSame($camel, Strings::camelCase($text));
        $this->assertSame($snake, Strings::snakeCase($text));
        $this->assertSame($kebab, Strings::kebabCase($text));
    }

}
