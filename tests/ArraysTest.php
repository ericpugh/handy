<?php

namespace Handy\Tests;

use PHPUnit\Framework\TestCase;
use Handy\Utils\Arrays;

/**
 * Class ArraysTest
 *
 * @package Handy\Tests
 */
class ArraysTest extends TestCase
{

    private $arr = ['two', 'three', 'four', 'five'];
    private $assocArr = [
        'float' => 1.099,
        'integer' => 2,
        'string' => 'three',
        'boolean' => false,
    ];
    private $multiDimArr = [
        'foo' => 'one',
        'bar' => ['two', 'three', 'four', 'five'],
        'baz' => [
            'foo' => [
                'bar' => 'six',
                'baz' => 'seven',
                'foo' => 'eight'
            ]
        ]
    ];

    /**
     * @test
     */
    public function flattenTest()
    {
        $flat = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight'];
        $this->assertSame($flat, Arrays::flatten($this->multiDimArr));
    }

    /**
     * @test
     */
    public function isAssocTest()
    {
        $this->assertTrue(Arrays::isAssoc($this->assocArr));
        $this->assertFalse(Arrays::isAssoc($this->arr));
    }

    /**
     * @test
     */
    public function getTest()
    {
        $this->assertSame($this->arr, Arrays::get($this->multiDimArr, 'bar'));
        $this->assertSame($this->assocArr['integer'], Arrays::get($this->assocArr, 'integer'));
        $this->assertSame('two', Arrays::get($this->arr, 0));
    }

}
