# Handy

A handy (and dead simple) PHP utility library. 

## Installation

Install with [composer](https://getcomposer.org/):

```
composer require ericpugh/handy
```

- requires with PHP 7.3 or greater.

## Utility Classes

* [Arrays](#arrays)
* [DateTimes](#datetimes)
* [Files](#files)
* [Markup](#markup)
* [Strings](#strings)
* [Validators](#validators)

## Methods

### Arrays
Array utilities.
* `::flatten()` Flatten a multi-dimensional array into a single level
* `::get()` Get a specific array value by key.
* `::isAssoc()` Check if an array is associative.

### DateTimes
Date and Time utilities.
* `::convertDateFormat()` Format a date string.

### Markup
Utilities working with HTML markup.
* `::clean()` Trim HTML tags/attributes from a string. 
* `::getUrl()` Get a URL from given HTML markup.

### Files
File system utilities.
* `::loadFile()` Load a local JSON file.

### Strings
String utilities.
* `::truncateText()` 

### Validators
Data validation utilities.
* `::isNull()` 

## Testing

``` bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
